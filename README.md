# google-map-sample-2
a simple google map sample with javascript with a separate json file

<a href="http://samplespace.atwebpages.com/sample2/">Demo</a>

Reference:
[Google Maps JS API v3 - Simple Multiple Marker Example] (http://stackoverflow.com/questions/3059044/google-maps-js-api-v3-simple-multiple-marker-example)
