//Global variables
var map;
var infoWindow;

function initialize() {
   var mapOptions = {
      center: new google.maps.LatLng(-33.92, 151.25),
      zoom: 10,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
   };
   map = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);

   infoWindow = new google.maps.InfoWindow();

   google.maps.event.addListener(map, 'click', function() {
      infoWindow.close();
   });

   displayMarkers();      
}
google.maps.event.addDomListener(window, 'load', initialize);

function displayMarkers(){
	
	var json = (function () { 
        var json = null; 
        $.ajax({ 
            'async': false, 
            'global': false, 
            'url': "data.json", 
            'dataType': "json", 
            'success': function (data) {
                json = data; 
            }
        });
        return json;
    })();
	
    var bounds = new google.maps.LatLngBounds();
    
	for (var i = 0, length = json.length; i < length; i++){

            var data = json[i],
            latLng = new google.maps.LatLng(data.lat, data.lng); 
			content = data.content;

        createMarker(latLng, content);
        bounds.extend(latLng);  
    }//end of for loop
	
	map.fitBounds(bounds);
}

function createMarker(latLng, content){
    var marker = new google.maps.Marker({
      map: map,
      position: latLng,
	  title: content
    });

    google.maps.event.addListener(marker, 'click', function() {
        var iwContent = content;
        infoWindow.setContent(iwContent);
        infoWindow.open(map, marker);
   });
}